# frozen_string_literal: true

require 'rails_helper'
describe TrelloProviderApi do
  describe '#list_details' do
    let(:board_id) {"ABC123"}
    let(:response) { file_fixture('trello_get_response.json').read }
    context 'Succesfully calls the api for details' do
      before(:each) do
        stub_request(:get, "https://api.com/1/boards/#{board_id}/lists?card_fields=name&cards=open").
          to_return(status: 200, body: response, headers: {})
      end

      it 'returns the list names' do
        client = described_class.new
        expect(client.list_details.first[:name]).to eq 'To do'
        expect(client.list_details.last[:name]).to eq 'Doing'
      end
  
      it 'returns the card details' do
        client = described_class.new
        todo_list_cards = client.list_details.first[:cards]
        doing_list_cards = client.list_details.last[:cards]
        expect(todo_list_cards.first[:name]).to eq 'Put shirts in oven?'
        expect(doing_list_cards.first[:name]).to eq 'Watch Youtube videos on making shirt pizzas'
      end
    end

    context 'Fails' do
      it 'raises an error on api timeout' do
        stub_request(:get, "https://api.com/1/boards/#{board_id}/lists?card_fields=name&cards=open").to_timeout
        client = described_class.new
        expect{ client.list_details.first[:name] }.to raise_error TrelloProviderApi::ApiError, 'Error occured: execution expired'
      end

      it 'raises an error if it can\'t find the board' do
        stub_request(:get, "https://api.com/1/boards/#{board_id}/lists?card_fields=name&cards=open").
          to_return(status: 404, body: 'board not found'.to_json, headers: {})

        client = described_class.new
        expect{ client.list_details.first[:name] }.to raise_error TrelloProviderApi::ApiError, 'Error occured: Invalid board ID'
      end
    end
  end

  describe '#create_card' do
    let(:response) { file_fixture('trello_post_response.json').read }
    context 'Success' do
      it 'Adds a card to the list' do
        stub_request(:post, "https://api.com/1/cards?idList=id&name=hello").
          to_return(status: 200, body: response, headers: {})
        client = described_class.new
        result = client.create_card('id', 'hello')
        expect(result[:idList]).to eq '5fc48c1a8241264b2f315aca'
        expect(result[:name]).to eq 'hello'
      end
    end
    context 'Fails' do
      it 'Raises an error on timeout' do
        stub_request(:post, "https://api.com/1/cards?idList=id&name=hello").to_timeout
        client = described_class.new
        expect{ client.create_card('id', 'hello') }.to raise_error TrelloProviderApi::ApiError, 'Error occured: execution expired'
      end

      it 'Raises an error when it can\'t find the board' do
        stub_request(:post, "https://api.com/1/cards?idList=id&name=hello").
          to_return(status: 404, body: 'board not found'.to_json, headers: {})
        client = described_class.new
        expect{ client.create_card('id', 'hello') }.to raise_error TrelloProviderApi::ApiError, 'Unable to create card: "board not found"'
      end
    end
  end
end