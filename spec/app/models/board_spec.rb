# frozen_string_literal: true

describe Board do
  describe '#lists' do
    let(:board_id) {"ABC123"}
    let(:response) { file_fixture('trello_get_response.json').read }
    before(:each) do
      stub_request(:get, "https://api.com/1/boards/ABC123/lists?card_fields=name&cards=open").
        to_return(status: 200, body: response, headers: {})
    end

    it 'returns list id' do
      expect(Board.lists.keys.first).to eq '5fbf4538bd825b05cc74e13b'
    end
    
    it 'returns list name' do
      expect(Board.lists.first.last[:list_name]).to eq 'To do'
    end

    it 'returns lists with array of cards' do
      expect(Board.lists.first.last[:cards].first).to eq 'Put shirts in oven?'
    end
  end

  describe '#create_card' do
    let(:post_response) { file_fixture('trello_post_response.json').read }
    it 'returns card details on creation' do
      stub_request(:post, "https://api.com/1/cards?idList=id&name=hello").
        to_return(status: 200, body: post_response, headers: {})
      result = Board.create_card(list_id: 'id', card_details: 'hello')
      expect(result[:idList]).to eq '5fc48c1a8241264b2f315aca'
      expect(result[:name]).to eq 'hello'
    end

    it 'returns false if list_id blank' do
      expect(Board.create_card(list_id: '', card_details: 'hello')).to eq false
    end

    it 'returns false if card_details are blank' do
      expect(Board.create_card(list_id: 'id', card_details: '')).to eq false
    end
  end
end
