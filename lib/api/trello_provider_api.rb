# frozen_string_literal: true

require 'faraday_middleware'

class TrelloProviderApi
  class ApiError < StandardError
    class NotFound < StandardError; end
  end

  def initialize
    @url = ENV.fetch('TRELLO_URL', 'https://api.com')
    @consumer_key = ENV.fetch('TRELLO_CONSUMER_KEY', 'somekey')
    @token = ENV.fetch('TRELLO_TOKEN', 'token')
    @token_secret = ''
    @consumer_secret = ''
    @board_id = ENV.fetch('BOARD_ID', 'ABC123')
  end

  def list_details
    response = connection.get "/1/boards/#{@board_id}/lists?cards=open&card_fields=name"
    raise ApiError::NotFound, 'Invalid board ID' if response.status == 404

    JSON.parse(response.body, symbolize_names: true)
  rescue StandardError => e
    raise ApiError, "Error occured: #{e.message}"
  end

  def create_card(list_id, card_details)
    response = post('/1/cards', { idList: list_id, name: card_details })
    status = response.status
    body = response.body
    return JSON.parse(body, symbolize_names: true) if status == 200

    raise ApiError, "Unable to create card: #{body}"
  end

  private

  def post(path, body)
    connection.post do |req|
      req.url path, body
      req.headers['Content-Type'] = 'application/json'
      req.body = body.to_json
    end
  rescue StandardError => e
    raise ApiError, "Error occured: #{e.message}"
  end

  def connection
    Faraday.new(url: @url, ssl: { verify: false }) do |conn|
      credentials = {
        consumer_key: @consumer_key,
        consumer_secret: @consumer_secret,
        token: @token,
        token_secret: @token_secret
      }
      conn.request :oauth, credentials
      conn.adapter Faraday.default_adapter
    end
  end
end
