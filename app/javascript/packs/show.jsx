import React , { createRef } from 'react';
import ReactDOM from 'react-dom';
import { Toolbar,Typography,Button, Container, Card, Grid ,Input, Modal, AppBar, Paper } from '@material-ui/core';
import { StylesProvider } from "@material-ui/core/styles";
import AddIcon from '@material-ui/icons/Add';
import LocalPizzaSharpIcon from '@material-ui/icons/LocalPizzaSharp';

class Lists extends React.Component {

  constructor(props) {
    super(props);
    this.new_card = createRef();
    this.state = {
      show: false,
      new_card: '',
      result: '',
      list_id: '',
      lists: props.lists,
      first_list: Object.values(props.lists)[0]['list_name'],
      first_list_cards: Object.values(props.lists)[0]['cards'],
      first_list_id: Object.keys(props.lists)[0],
      second_list: Object.values(props.lists)[1]['list_name'],
      second_list_cards: Object.values(props.lists)[1]['cards'],
      second_list_id: Object.keys(props.lists)[1],
      loading: false
    };
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
    
    // regex
    this[event.target.name].current.hidden = true;
    var result = (event.target.name === 'new_card') 
    ? event.target.value.match(/[~!@#$%^&*():{}|<>-\s]/g) 
    : event.target.value === '';

    if(result) {
      this[event.target.name].current.hidden = false;
      (event.target.name === 'new_card')
      ? this[event.target.name].current.innerText = 'Special characters not allowed.'
      : this[event.target.name].current.innerText = 'Please specify some text'
    }
  }

  actionHandler = (key) => {
    // set state
    this.setState({ result: '', message: '', loading: true, [key]: true });
    // create data obj for post
    const data = {
      list_id: this.state.list_id,
      card_details: this.state.new_card
    }
    // declare async
    async function postData(url = '', data = {}) {
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data) 
      });
      return await response.json();
    }
    // call async with data
    postData(`/lists/${key}`, data)
    .then((result) => {
      const props = { loading: false, [key]: false }
      if(result.message) { this.setState({ message: result.message, ...props }); }
      if(result.data) { this.setState({ result: result.data, ...props }); }
      if(this.state.lists[result.data.idList]['list_name'] == 'To do') {
        this.setState({first_list_cards: this.state.first_list_cards.concat([result.data.name])});
      }
      else{
        this.setState({second_list_cards: this.state.second_list_cards.concat([result.data.name])});
      }
      console.log(result);
    });
  }

  showModal = (id) => {
    this.setState({ list_id: id });
    this.setState({ show: true });
  };

  hideModal = () => {
    this.setState({ show: false });
  };

  render() {
    const modalBody = (
    <Card className='modal'>
      <Input intent="default"
            label="Project name*"
            onChange={(event) => this.handleChange(event)}
            type="text"
            variant="default"
            value={this.state.new_card}
            name="new_card" />
            <div ref={this.new_card} hidden={true}>Hello</div>
            <Button size="small" type="button" 
            onClick={() => this.actionHandler('create_card')}>Submit Card</Button>
            {this.state.message && 
            <div className='result'>
              {this.state.message}
            </div>
          }

          {(this.state.loading || this.state.result) && 
            <div className='result'>
              {this.state.new_card ? 'Adding Card...' : ''}
              <p>{this.state.result.name ? 'Success' : ''}</p>
            </div>
          }
    </Card>
  );
    return (
    <StylesProvider injectFirst>
      <AppBar position="static" >
        <Toolbar className='toolbar'>
          <Typography variant="h6">
            The Pizza/Shirt Project
          </Typography>
          <LocalPizzaSharpIcon className='pizzaIcon'/>
        </Toolbar>
        </AppBar>
      <Container>
      <Grid container alignItems="stretch" spacing={3}>
          <Grid item md={4} className='cardList'>
            <Grid container direction={'row'}>
              <Grid item xs={6}>
              <Typography variant="h6">
                {this.state.first_list}
              </Typography>
              </Grid>
              <Grid container item xs={6} justify="flex-end">
              <Button  className="addButton" variant="contained" size="small" 
              onClick={() => this.showModal(this.state.first_list_id)} startIcon={<AddIcon />}>
                Add
              </Button>
              </Grid>
            </Grid>
        {this.state.first_list_cards.map((card, i) => {
          return <Card className="card" key={i}>{card}</Card>;
        })}
          </Grid>
          <Grid item md={4} className='cardList'>
            <Grid container direction={'row'}>
              <Grid item xs={6}>
              <Typography variant="h6">
               {this.state.second_list}
              </Typography>
              </Grid>
            <Grid container item xs={6} justify="flex-end">
            <Button className="addButton" variant="contained" size="small" 
            onClick={() => this.showModal(this.state.second_list_id)} startIcon={<AddIcon />}>
              Add
            </Button>
            </Grid>
            </Grid>
            {this.state.second_list_cards.map((card, i) => {
              return <Card className="card" key={i}>{card}</Card>;
            })}
          </Grid>
        </Grid>
        </Container>
        <Modal open={this.state.show} onClose={this.hideModal} >{modalBody}</Modal>
        </StylesProvider>
    );
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('list')
  const data = JSON.parse(node.getAttribute('data'))

ReactDOM.render(
  <Lists lists={data} />,
  document.body.appendChild(document.createElement('div')),
)
});